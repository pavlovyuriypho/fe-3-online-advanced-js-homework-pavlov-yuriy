"use strict"

class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }
    get name () {
        return this.name;
    }
    set name (name) {
        this.name = name;
    }
    get age () {
        return this.age;
    }
    set age (age) {
        this.age = age;
    }
    get salary () {
        return this.salary;
    }
    set salary (salary) {
        this.salary = salary;
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary * 3);
        this.lang = lang;
    }
    get salary () {
        return this.salary;
    }
}

const programmerOne = new Programmer ("Anna", 31, 3000, "cn");
console.log(programmerOne);

const programmerTwo = new Programmer ("Dmitriy", 38, 5000, "fr");
console.log(programmerTwo);

const programmerThree = new Programmer ("Fedor", 37, 4000, "gr");
console.log(programmerThree);