"use strict";

const ipUrl = 'https://api.ipify.org/?format=json';
const userUrl = 'http://ip-api.com/json/';
const btn = document.querySelector('.btnSearch');
const root = document.querySelector('.root');
btn.addEventListener('click', (e) => {
   async function getIp() {
       const request = await fetch(ipUrl);
       const requestJson = await request.json();
       const requestIp = await fetch(userUrl);
       const requestIpJson = await requestIp.json();
       root.insertAdjacentHTML("afterend",
           `<div class="wrapper">
                     <p class="page-text__user">Information about you:</p>                    
                     <p class="page-text">Country code: ${requestIpJson.countryCode}</p>
                     <p class="page-text">Country: ${requestIpJson.country}</p>
                     <p class="page-text">Region: ${requestIpJson.region}</p>
                     <p class="page-text">City: ${requestIpJson.city}</p>
                     <p class="page-text">Region name: ${requestIpJson.regionName}</p>
                     <p class="page-text">Your IP: ${requestJson.ip}</p>
                 </div>`);
       };
    getIp();
});