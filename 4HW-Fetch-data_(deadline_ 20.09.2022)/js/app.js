"use strict"

const filmsUrl = "https://ajax.test-danit.com/api/swapi/films";

fetch(filmsUrl)
    .then((data) => {
        return data.json();
    })
    .then((data) => {
data.forEach((e) => {
    document.body.insertAdjacentHTML('afterbegin', `<p>${e.episodeId}</p>
    <p>${e.name}</p><p>${e.openingCrawl}</p><p id="characters"></p>`);

const parent = document.querySelector('#characters');
    e.characters.forEach((url) => {
        fetch(url)
            .then((rest) => {
                return rest.json();
            })
            .then((rest) => {
                parent.insertAdjacentHTML("afterend", `<div>${rest.name}</div>`)
            });
        });
    });
});