"use strict"

const books = [
    {
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    },
    {
        name: "Тысячекратная мысль",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
    },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    }
];

const rootElement = document.createElement('div');
rootElement.setAttribute('id', "root");
document.body.append(rootElement);
const booksList = document.createElement('ul');
rootElement.prepend(booksList);

books.forEach( (item) => {
    try {
        if (!item.author) {
            throw new SyntaxError("Ошибка, нет автора.");
        } else if (!item.name) {
            throw new SyntaxError("Ошибка, нет названия книги.");
        }  else if (!item.price) {
            throw new SyntaxError("Ошибка, нет цены.");
        }
        booksList.insertAdjacentHTML('beforeend',
            `<li>${item.author}, ${item.name }, ${item.price}</li>`);
    } catch (e) {
        console.log(e.message);
    }
});